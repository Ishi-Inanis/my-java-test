package test.edu.game.creature;

import edu.game.world.creature.Cat;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class CatTest {
    @Test
    public void testInit() {
        Cat cat = new Cat("Cat");

        assertEquals("Cat", cat.getName());
        assertEquals(10, cat.getHp());
        assertTrue(cat.getDamage() > 0);
        assertTrue(cat.getAttackSpeed() >= 1 & cat.getAttackSpeed() <= 2);
        assertNotNull(cat.toString());

        System.out.println(cat);
    }

    @Test
    public void testFight() {
        Cat[] cats = {new Cat("Нуну"), new Cat("Вилумп")};

        int step = 0;

        while (cats[0].isLive() & cats[1].isLive()) {
            if (step++ % 2 == 0)
                cats[0].hit(cats[1]);
            else
                cats[1].hit(cats[0]);
        }

        if (cats[1].isLive())
            cats[0] = null;
        else
            cats[1] = null;
    }
}