package edu.game;

import edu.game.world.Time;
import edu.game.world.creature.Cat;
import edu.game.world.creature.Creature;
import edu.game.world.creature.Human;

public class Main {
    public static void main(String[] args) {
        System.out.println("=== Добро пожаловать ===");
        System.out.println();

        Human humanBob = new Human("Боб");

        catVsHuman();

        System.out.println();
        System.out.println(humanBob);
        System.out.println();
        outAllCount();
    }

    public static void catVsHuman() {
        Cat cat = new Cat("Рыжик");
        Human human = new Human("Вася");

        while (cat.isLive() & human.isLive()) {
            if (Time.getStep() % human.getAttackSpeed() == 0)
                human.hit(cat);
            if (Time.getStep() % cat.getAttackSpeed() == 0 & cat.isLive())
                cat.hit(human);
            Time.stepForward();
        }

        if (!cat.isLive())
            cat = null;

        if (!human.isLive())
            cat = null;
    }

    public static void outAllCount() {
        System.out.println("Всего котов: " + Cat.getCount());
        System.out.println("Всего людей: " + Human.getCount());
        System.out.println("Всего существ: " + Creature.getCount());
    }
}
