package edu.game.world.creature;

public abstract class Creature {
    protected String name;
    protected int hp;
    protected int damage;
    protected int attackSpeed; // double
    private static int count = 0;

    public Creature(String name) {
        this.name = name;
        count++;
    }

    public String getName() {
        return name;
    }

    public int getHp() {
        return hp;
    }

    public int getDamage() {
        return damage;
    }

    public int getAttackSpeed() {
        return attackSpeed;
    }

    public boolean isLive() {
        return hp > 0;
    }

    public static int getCount() {
        return count;
    }

    public void hit(Creature creature) {
        creature.hp -= damage;
        System.out.println(name + " бьет " + creature.name + " и снимает " + damage + " HP");

        if (!creature.isLive())
            creature.die();
    }

    @Override
    public String toString() {
        return String.format("""
                Существо {
                     "name": %s,
                     "hp": %d,
                     "damage": %d,
                     "attackSpeed": %d,
                 }
                 """, name, hp, damage, attackSpeed);
    }

    protected void die() {
        count--;
        System.out.println(name + " мертв");
    }
}
