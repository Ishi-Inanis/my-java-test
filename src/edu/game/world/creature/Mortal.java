package edu.game.world.creature;

public interface Mortal {
    boolean isLive();
    void die();
}
