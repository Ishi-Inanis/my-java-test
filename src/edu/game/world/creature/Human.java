package edu.game.world.creature;

import java.util.Random;

public class Human extends Creature {
    private static int count = 0;

    public Human(String name) {
        super(name);
        super.hp = 100;
        super.damage = new Random().nextInt(10) + 1;
        super.attackSpeed = new Random().nextInt(3) + 3; // 3-5
        count++;
    }

    public static int getCount() {
        return count;
    }

    @Override
    public String toString() {
        return String.format("""
                Человек {
                     "name": %s,
                     "hp": %d,
                     "damage": %d,
                     "attackSpeed": %d,
                 }
                 """, name, hp, damage, attackSpeed);
    }

    @Override
    protected void die() {
        super.die();
        count--;
    }
}
