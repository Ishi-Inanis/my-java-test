package edu.game.world.creature;

import java.util.Random;

public class Cat extends Creature {
    private static int count = 0;

    public Cat(String name) {
        super(name);
        super.hp = 10;
        super.damage = new Random().nextInt(10) + 1;
        super.attackSpeed = new Random().nextInt(2) + 1; // 1-2
        count++;
    }

    public static int getCount() {
        return count;
    }

    @Override
    public String toString() {
        return String.format("""
                Кот {
                     "name": %s,
                     "hp": %d,
                     "damage": %d,
                     "attackSpeed": %d,
                 }
                 """, name, hp, damage, attackSpeed);
    }

    @Override
    protected void die() {
        super.die();
        count--;
    }
}