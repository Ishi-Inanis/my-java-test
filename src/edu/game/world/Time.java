package edu.game.world;

public class Time {
    protected static int step = 0;

    public static int getStep() {
        return step;
    }

    public static void stepForward() {
        step++;
    }
}
