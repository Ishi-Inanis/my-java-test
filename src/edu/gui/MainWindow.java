package edu.gui;

import edu.game.world.Time;
import edu.game.world.creature.Creature;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class MainWindow extends JFrame {
    private Creature creature1;
    private Creature creature2;

    private JPanel panelMain;
    private JButton startButton;
    private JProgressBar progressBar1;
    private JProgressBar progressBar2;
    private JLabel labelName1;
    private JLabel labelName2;
    private JLabel labelHp1;
    private JLabel labelHp2;
    private JLabel LabelDmg1;
    private JLabel labelDmg2;
    private JLabel labelDmgValue1;
    private JLabel labelDmgValue2;
    private JLabel labelAspd1;
    private JLabel labelAspd2;
    private JLabel labelAspdValue1;
    private JLabel labelAspdValue2;

    public MainWindow(String title, Creature creature1, Creature creature2) {
        super(title);

        this.creature1 = creature1;
        this.creature2 = creature2;

        startButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                if (startButton.isEnabled()) fight();
            }
        });

        progressBar1.setStringPainted(true);
        progressBar1.setMinimum(0);
        progressBar1.setMaximum(100);
        progressBar1.setValue(creature1.getHp());

        progressBar2.setStringPainted(true);
        progressBar2.setMinimum(0);
        progressBar2.setMaximum(100);
        progressBar2.setValue(creature2.getHp());

        labelName1.setText(creature1.getName());
        labelName2.setText(creature2.getName());

        labelDmgValue1.setText(String.valueOf(creature1.getDamage()));
        labelDmgValue2.setText(String.valueOf(creature2.getDamage()));

        labelAspdValue1.setText(String.valueOf(creature1.getAttackSpeed()));
        labelAspdValue2.setText(String.valueOf(creature2.getAttackSpeed()));

        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        setContentPane(panelMain);
        pack();
    }

    public void fight() {
        while (creature1.isLive() & creature2.isLive()) {
            if (Time.getStep() % creature2.getAttackSpeed() == 0)
                creature2.hit(creature1);
            if (Time.getStep() % creature1.getAttackSpeed() == 0 & creature1.isLive())
                creature1.hit(creature2);
            Time.stepForward();
        }

        progressBar1.setValue(creature1.getHp());
        progressBar2.setValue(creature2.getHp());

        if (!creature1.isLive() | !creature2.isLive()) startButton.setEnabled(false);

        if (!creature1.isLive()) {
            creature1 = null;
            labelName1.setText(labelName1.getText() + " (Мертв)");
        }

        if (!creature2.isLive()) {
            creature2 = null;
            labelName2.setText(labelName2.getText() + " (Мертв)");
        }
    }
}
