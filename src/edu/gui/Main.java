package edu.gui;

import edu.game.world.creature.Human;

import javax.swing.*;

public class Main {
    public static void main(String[] args) {
        Human human1 = new Human("Вася");
        Human human2 = new Human("Иван");

        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
//            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            e.printStackTrace();
        }

        new MainWindow("Game GUI", human1, human2);
    }
}
