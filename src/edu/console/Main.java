package edu.console;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.util.*;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        arrays();
    }

    public static void labels(String[] args) {
        boolean i = false;

        label1: {
            System.out.println(1);
            if (i) break label1;
            label2: {
                System.out.println(2);
                if (!i) {
                    i = true;
                    break label2;
                }
                System.out.println(3);
            }
            System.out.println(4);
        }

        Runnable r = () -> {
            System.out.println(0);
        };

        r.run();

        String c = "John";
        String s = String.format("""
                {
                    "name": %s,
                }
                """, c);
        System.out.println(s);
    }

    public static void arrays() throws InterruptedException {
        ArrayList<Integer> list = new ArrayList<Integer>();
        Map<String, Date> map = new HashMap<String, Date>();
        Set<String> set = new HashSet<String>();

        map.put("one date", new Date());
        Thread.sleep(3000);
        map.put("two date", new Date());

        for (Date date : map.values())
            System.out.println(date);

        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);

        Iterator<Integer> i = list.iterator();
        while (i.hasNext())
            System.out.println(i.next());
    }

    public static void test() throws IOException {
        int i = 1,
                a = 0;
        double d = 1.0;
        String str = "qwe";

        InputStream iS = System.in;
        Reader iSReader = new InputStreamReader(iS);
        BufferedReader bR = new BufferedReader(iSReader);
        String name = bR.readLine();
        System.out.println(name);

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        Scanner scanner = new Scanner(System.in);

        int[] arrInt = {1, 2, 3};

        str = i + str + d;

        System.out.println(a);
        System.out.println(i);
        System.out.println(d);
        System.out.println(str);
        System.out.println(Arrays.toString(arrInt));
        System.out.println(new Date());
    }
}